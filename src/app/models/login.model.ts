export class LoginInputs {
    name: string;
    surname: string;
    iban: string;
}