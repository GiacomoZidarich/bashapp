import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataReviewPage } from './data-review.page';

const routes: Routes = [
  {
    path: '',
    component: DataReviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataReviewPageRoutingModule {}
