import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DataReviewPage } from './data-review.page';

describe('DataReviewPage', () => {
  let component: DataReviewPage;
  let fixture: ComponentFixture<DataReviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DataReviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
