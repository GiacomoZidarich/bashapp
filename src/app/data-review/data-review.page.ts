import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoginInputs } from '../models/login.model';

@Component({
  selector: 'app-data-review',
  templateUrl: './data-review.page.html',
  styleUrls: ['./data-review.page.scss'],
})
export class DataReviewPage implements OnInit {

  loginData: string[];

  constructor(private router: ActivatedRoute)
   {
    this.router.params.subscribe(
      (loginDataParam: LoginInputs) => {
        this.loginData = [loginDataParam.name, loginDataParam.surname, loginDataParam.iban];       
      }
    );
  }

  ngOnInit() {
  }

}
