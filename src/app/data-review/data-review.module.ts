import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataReviewPageRoutingModule } from './data-review-routing.module';

import { DataReviewPage } from './data-review.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataReviewPageRoutingModule
  ],
  declarations: [DataReviewPage]
})
export class DataReviewPageModule {}
