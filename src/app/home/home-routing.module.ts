import { NgModule } from '@angular/core';
import { Routes, RouterModule, ChildrenOutletContexts } from '@angular/router';
import {HomePage} from './home.page'

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children:[{

        //to Log In page
        path: 'login',
        children: [{
            path: '',
            loadChildren: () =>
                import('../login/login.module').then(m => m.LoginPageModule)
        }]
    }]
  },
  {
      path: '',
      redirectTo: '/home/login',
      pathMatch: 'full'
  }
];

@NgModule({ 
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
