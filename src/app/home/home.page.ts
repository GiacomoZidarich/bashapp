import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  constructor(public alertController: AlertController) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Fantastico!',
      subHeader: 'Ti sta piacendo?',
      message: 'Hai visto quanto è bello questo alert?',
      buttons: ['sì', 'Ovvio che sì']
    });

    await alert.present();
  }
}
