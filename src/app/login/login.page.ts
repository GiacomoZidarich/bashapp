import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginInputs } from '../models/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router) { }

  formData: LoginInputs = new LoginInputs();

  ngOnInit() {
  }

  CheckDataPresence() {
    return (!this.formData.iban || !this.formData.name || !this.formData.surname);
  }

  NavigateToReview() {
    this.router.navigate(['/data-review', this.formData]);
  }

}
